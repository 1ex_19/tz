@extends('layout')
@section('content')

@if (session('text'))

<div data-aos="fade" data-aos-duration="500" class="card">
    <div class="card-header">Your text:</div>
    <div class="card-body">
        <p>{{session('text')}}</p>
    </div>
</div>

@else

<form data-aos="fade" data-aos-duration="500" method="POST" action="/secret/{{$id}}/check">
    @csrf
    <div class="m-3">
        <label class="form-label" for="phrase">Passphrase</label>
        <input autocomplete="off" type="text" name="phrase" id="phrase" value="{{old('phrase')}}"
            class="form-control @error('phrase') is-invalid @enderror">

        @error('phrase')
        <div class="invalid-feedback">
            @lang($message)
        </div>
        @enderror
        
    </div>
    <div class="m-3">
        <button type="submit" class="btn btn-primary">Submith</button>
    </div>
</form>

@endif
@endsection