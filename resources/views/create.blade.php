@extends('layout')
@section('content')

@if (session('message'))
<div class="position-fixed bottom-0 end-0 p-3" style="z-index: 11">
    <div class="toast fade show" role="alert" aria-live="assertive" aria-atomic="true">
        <div class="toast-header">
            <strong class="me-auto">Secret keeper</strong>
            <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
        </div>
        <div class="toast-body">
            {{session('message')}}
        </div>
    </div>
</div>
@endif


@if (session('url'))

<div data-aos="fade" data-aos-duration="500" class="card">
    <div class="card-header">Your link:</div>
    <div class="card-body">
        <p>{{session('url')}}</p>
        <a href="/secret/create" class="btn btn-primary">Create another one</a>
        <a href="{{session('url')}}" class="btn btn-success">Go</a>
    </div>
</div>

@else

<form data-aos="fade" data-aos-duration="500" method="POST" action="/secret">
    @csrf
    <div class="m-3">
        <label class="form-label" for="text">Secret text</label>
        <textarea autocomplete="off" name="text" id="text" value="{{old('text')}}"
            class="form-control @error('text') is-invalid @enderror"></textarea>

        @error('text')
        <div class="invalid-feedback">
            @lang($message)
        </div>
        @enderror

    </div>
    <div class="m-3">
        <label class="form-label" for="phrase">Passphrase</label>
        <input autocomplete="off" type="text" name="phrase" id="phrase" value="{{old('phrase')}}"
            class="form-control @error('phrase') is-invalid @enderror">

        @error('phrase')
        <div class="invalid-feedback">
            @lang($message)
        </div>
        @enderror
        
    </div>
    <div class="m-3">
        <button type="submit" class="btn btn-primary">Submith</button>
    </div>
</form>

@endif

@endsection
