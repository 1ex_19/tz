<?php

namespace App\Http\Controllers;

use App\Http\Requests\SecretCheckRequest;
use App\Models\Secret;
use App\Http\Requests\StoreSecretRequest;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;

class SecretController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreSecretRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSecretRequest $request)
    {
        // проверенные и отфилтрованные поля.
        // $validated_fields = $request->validate([
        //     'text' => 'required|string',
        //     'phrase' => 'required|string|max:191|min:5'
        // ]);

        // плохой вариант
        // if(strlen($request->text) > 191){
        //     return redirect()->back()->with([
        //         'error' => "text is more the 191"
        //     ])
        // }
        // if(strlen($request->text) < 5){
        //     return redirect()->back()->with([
        //         'error' => "text is less the 5"
        //     ])
        // }
        $secret = Secret::create($request->validated());
        return redirect()->back()->with(['url' => URL::route('secret.show', $secret->id)]);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id, $delete = null)
    {
        if ($secret = Secret::find($id)) {

            if ($delete !== null && session()->has('text')) $secret->delete();

            return view('show', [
                'id' => $id
            ]);
        } else return redirect()->route('secret.create')
            ->with('message', __("These aren't the droids you're looking for."));
    }

    /**
     * Check the specified resource in storage.
     *
     * @param  \App\Http\Requests\SecretCheckRequest  $request
     * @param  \App\Models\Secret  $secret
     * @return \Illuminate\Http\Response
     */
    public function check(SecretCheckRequest $request, Secret $secret)
    {
        if (Hash::check($request->phrase, $secret->phrase)) {
            return redirect()->route('secret.show', ['secret' => $secret, 'delete' => 'delete'])
            ->with(['text' => Crypt::decryptString($secret->text)]);
        } else {
            $secret->delete();
            return redirect()->route('secret.create')->with('message', __('You have failed this sity..'));
        }
    }
}
