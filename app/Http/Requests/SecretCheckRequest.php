<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SecretCheckRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phrase' => 'required|string|max:191|min:5'
        ];
    }
}
