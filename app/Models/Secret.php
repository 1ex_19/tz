<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class Secret extends Model
{
    use HasFactory;

    protected $fillable = [
        'text',
        'phrase'
    ];


    public function setTextAttribute($value)
    {
        $this->attributes['text'] = Crypt::encryptString($value);
    }

    public function setPhraseAttribute($value)
    {
        $this->attributes['phrase'] = Hash::make($value);
    }

}
