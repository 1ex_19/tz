<?php

use App\Http\Controllers\SecretController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('secret.create');
})->name('main');

Route::resource('secret', SecretController::class)
    ->only([
        'create', 'store'
    ]);
Route::post('secret/{secret}/check', [SecretController::class, 'check'])->name('secret.check');

// переопределяем маршрут, т.к. хотим изменить поведение по умолчанию для 404 ошибки
Route::get('secret/{secret}/{delete?}', [SecretController::class, 'show'])->name('secret.show');
